package com.zettelnet.armorweight;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.LazyMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import com.zettelnet.armorweight.event.HorseWeightChangeEvent;
import com.zettelnet.armorweight.event.PlayerWeightChangeEvent;
import com.zettelnet.armorweight.lib.darkblade12.ReflectionHandler;
import com.zettelnet.armorweight.lib.darkblade12.ReflectionHandler.PackageType;
import com.zettelnet.armorweight.lib.darkblade12.ReflectionHandler.SubPackageType;

/**
 * 
 * A manager used to calculate weight and update the effects of players and
 * horses.
 * <p>
 * Players should only be loaded in enabled worlds but this manager will not
 * take care of that and enabling and disabling worlds will not cause this
 * manager to load or unload players. <br>
 * Horses should only only be loaded if they are ridden by a loaded player, but
 * this manager will not take care of that. <br>
 * The ArmorWeight default listener will load and unload players and horses
 * appropriately.
 * 
 * @author Zettelkasten
 *
 */
public class WeightManager {

	protected abstract class WeightData<T extends Entity> {

		private final T entity;

		private double weight;
		private boolean dirty;

		private final MetadataValue metadata;

		public WeightData(T entity, double weight) {
			this(entity, weight, true);
		}

		public WeightData(T entity, double weight, boolean dirty) {
			this.entity = entity;
			this.weight = weight;
			this.dirty = dirty;

			this.metadata = makeWeightMetadata(this);
		}

		public T getEntity() {
			return entity;
		}

		public double getStoredWeight() {
			return weight;
		}

		public boolean updateWeight(final double weight) {
			if (this.weight != weight) {
				Bukkit.getPluginManager().callEvent(makeEvent(this.weight, weight));
				this.weight = weight;
				setDirty(true);
			}
			boolean dirty = isDirty();
			if (dirty) {
				metadata.invalidate();
			}
			return dirty;
		}

		protected abstract Event makeEvent(double oldWeight, double newWeight);

		public boolean isDirty() {
			return dirty;
		}

		public void setDirty(boolean dirty) {
			this.dirty = dirty;
		}

		protected void init() {
			entity.setMetadata(METADATA_KEY, metadata);
		}

		protected void destroy() {
			entity.removeMetadata(METADATA_KEY, metadata.getOwningPlugin());
		}
	}

	protected class PlayerData extends WeightData<Player> {

		private boolean hadSpeed;

		public PlayerData(final Player player, double weight) {
			this(player, weight, true);
		}

		public PlayerData(final Player player, double weight, boolean dirty) {
			super(player, weight, dirty);

			this.hadSpeed = true;
		}

		@Override
		protected Event makeEvent(double oldWeight, double newWeight) {
			return new PlayerWeightChangeEvent(getEntity(), oldWeight, newWeight);
		}

		public boolean hadSpeedEffect() {
			return hadSpeed;
		}

		public void setHadSpeedEffect(boolean hadSpeedEffect) {
			this.hadSpeed = hadSpeedEffect;
		}
	}

	protected class HorseData extends WeightData<Horse> {

		private double defaultSpeed;

		public HorseData(final Horse horse, double weight) {
			this(horse, weight, true);
		}

		public HorseData(final Horse horse, double weight, boolean dirty) {
			super(horse, weight, dirty);

			this.defaultSpeed = 0D;
		}

		public double getDefaultSpeed() {
			return defaultSpeed;
		}

		public void setDefaultSpeed(double speed) {
			this.defaultSpeed = speed;
		}

		@Override
		protected Event makeEvent(double oldWeight, double newWeight) {
			return new HorseWeightChangeEvent(getEntity(), oldWeight, newWeight);
		}
	}

	/*
	 * Creates a new LazyMetadataValue that returns the current weight of a
	 * WeightData.
	 */
	private final MetadataValue makeWeightMetadata(final WeightData<?> data) {
		return new LazyMetadataValue(getPlugin(), new Callable<Object>() {
			@Override
			public Object call() {
				return Double.valueOf(data.getStoredWeight());
			}
		});
	}

	/**
	 * The {@link MetadataValue} key for storing an {@link Entity}'s weight, usually
	 * <code>"weight"</code>.
	 * <p>
	 * <code>entity.getMetadata(<strong>METADATA_KEY</strong>)</code> will return
	 * the current weight of any loaded entity in this weight manager determined by
	 * the getWeight(..) methods. <br>
	 * The metadata value is owned by the plugin that owns this manager (
	 * {@link #getPlugin()}).
	 */
	public final static String METADATA_KEY = "weight";

	/**
	 * The weight at which a player has the same effects a vanilla Minecraft player
	 * (unnoticeable effects).
	 */
	public final static double DEFAULT_PLAYER_WEIGHT = 1D;

	/**
	 * The weight a player wearing no armor has.
	 */
	public static double PLAYER_WEIGHT = DEFAULT_PLAYER_WEIGHT;

	/**
	 * The weight at which a horse has the same effects a vanilla Minecraft horse
	 * (unnoticable effects) has.
	 */
	public final static double DEFAULT_HORSE_WEIGHT = 5D;

	/**
	 * The weight a horse wearing no armor has.
	 */
	public static double HORSE_WEIGHT = DEFAULT_HORSE_WEIGHT;

	/**
	 * The walk speed a player with default weight has.
	 * 
	 * @see #DEFAULT_PLAYER_WEIGHT
	 */

	public final static float DEFAULT_WALK_SPEED = 0.2F;
	/**
	 * The fly speed a player with default weight has.
	 * 
	 * @see #DEFAULT_PLAYER_WEIGHT
	 */
	public final static float DEFAULT_FLY_SPEED = 0.1F;

	/**
	 * The weight one level of an unspecified enchantment has. All unspecified
	 * enchantment weights are not contained in the enchantment weight map (
	 * {@link #ENCHANTMENT_WEIGHTS}).
	 * 
	 * @see #ENCHANTMENT_WEIGHTS
	 */
	public static double DEFAULT_ENCHANTMENT_WEIGHT = 0.01D;

	/**
	 * A map containing the weight for each enchantment per enchantment level. The
	 * map should not contain a <code>null</code> key, instead
	 * {@link #DEFAULT_ENCHANTMENT_WEIGHT} should be used for setting a default
	 * value.
	 * 
	 * @see #DEFAULT_ENCHANTMENT_WEIGHT
	 */
	public final static Map<Enchantment, Double> ENCHANTMENT_WEIGHTS = new HashMap<>();

	private class LoadedPlayersSet implements Set<Player> {

		private final Set<Player> base;

		private LoadedPlayersSet() {
			this.base = playerData.keySet();
		}

		@Override
		public int size() {
			return base.size();
		}

		@Override
		public boolean isEmpty() {
			return base.isEmpty();
		}

		@Override
		public boolean contains(Object o) {
			return base.contains(o);
		}

		@Override
		public Iterator<Player> iterator() {
			return new LoadedPlayersIterator();
		}

		private class LoadedPlayersIterator implements Iterator<Player> {

			private final Iterator<Player> i;

			private Player current;

			private LoadedPlayersIterator() {
				this.i = base.iterator();
			}

			@Override
			public boolean hasNext() {
				return i.hasNext();
			}

			@Override
			public Player next() {
				current = i.next();
				return current;
			}

			@Override
			public void remove() {
				if (current == null) {
					throw new IllegalStateException();
				}
				LoadedPlayersSet.this.remove(current);
			}

		}

		@Override
		public Object[] toArray() {
			return base.toArray();
		}

		@Override
		public <T> T[] toArray(T[] a) {
			return base.toArray(a);
		}

		@Override
		public boolean add(Player player) {
			if (loadPlayer(player)) {
				return base.add(player);
			} else {
				return false;
			}
		}

		@Override
		public boolean remove(Object o) {
			if (!(o instanceof Player)) {
				return false;
			} else {
				return unloadPlayer((Player) o);
			}
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			return base.containsAll(c);
		}

		@Override
		public boolean addAll(Collection<? extends Player> players) {
			boolean mod = false;
			for (Player player : players) {
				mod |= add(player);
			}
			return mod;
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			return base.retainAll(c);
		}

		@Override
		public boolean removeAll(Collection<?> objects) {
			boolean mod = false;
			for (Object obj : objects) {
				mod |= remove(obj);
			}
			return mod;
		}

		@Override
		public void clear() {
			for (Iterator<Player> i = iterator(); i.hasNext();) {
				remove(i.next());
			}
		}
	}

	private class LoadedHorsesSet implements Set<Horse> {

		private final Set<Horse> base;

		private LoadedHorsesSet() {
			this.base = horseData.keySet();
		}

		@Override
		public int size() {
			return base.size();
		}

		@Override
		public boolean isEmpty() {
			return base.isEmpty();
		}

		@Override
		public boolean contains(Object o) {
			return base.contains(o);
		}

		@Override
		public Iterator<Horse> iterator() {
			return new LoadedHorsesIterator();
		}

		private class LoadedHorsesIterator implements Iterator<Horse> {

			private final Iterator<Horse> i;

			private Horse current;

			private LoadedHorsesIterator() {
				this.i = base.iterator();
			}

			@Override
			public boolean hasNext() {
				return i.hasNext();
			}

			@Override
			public Horse next() {
				current = i.next();
				return current;
			}

			@Override
			public void remove() {
				if (current == null) {
					throw new IllegalStateException();
				}
				LoadedHorsesSet.this.remove(current);
			}

		}

		@Override
		public Object[] toArray() {
			return base.toArray();
		}

		@Override
		public <T> T[] toArray(T[] a) {
			return base.toArray(a);
		}

		@Override
		public boolean add(Horse horse) {
			if (loadHorse(horse)) {
				return base.add(horse);
			} else {
				return false;
			}
		}

		@Override
		public boolean remove(Object o) {
			if (!(o instanceof Horse)) {
				return false;
			} else {
				return unloadHorse((Horse) o);
			}
		}

		@Override
		public boolean containsAll(Collection<?> c) {
			return base.containsAll(c);
		}

		@Override
		public boolean addAll(Collection<? extends Horse> horses) {
			boolean mod = false;
			for (Horse horse : horses) {
				mod |= add(horse);
			}
			return mod;
		}

		@Override
		public boolean retainAll(Collection<?> c) {
			return base.retainAll(c);
		}

		@Override
		public boolean removeAll(Collection<?> objects) {
			boolean mod = false;
			for (Object obj : objects) {
				mod |= remove(obj);
			}
			return mod;
		}

		@Override
		public void clear() {
			for (Iterator<Horse> i = iterator(); i.hasNext();) {
				remove(i.next());
			}
		}
	}

	private final Plugin plugin;
	private final Logger logger;

	private Set<String> enabledWorlds;
	private boolean allWorldsEnabled;

	private boolean playerArmorWeightEnabled;
	private boolean horseArmorWeightEnabled;
	private boolean horsePassengerWeightEnabled;
	private boolean enchantmentWeightEnabled;

	private boolean playerSpeedEffectEnabled;
	private boolean playerCreativeSpeedEffectEnabled;
	private boolean horseSpeedEffectEnabled;
	private double speedEffectAmplifier = 1.0;

	private boolean playerKnockbackEffectEnabled;
	private double knockbackEffectAmplifier = 1.0;

	private boolean portableHorsesEnabled;

	private final Map<Player, PlayerData> playerData;
	private final Set<Player> loadedPlayers;

	private final Map<Horse, HorseData> horseData;
	private final Set<Horse> loadedHorses;

	private Class<?> craftHorseClass;
	private Class<?> entityHorseClass;
	private Class<?> iAttributeClass;
	private Class<?> genericAttributesClass;
	private Class<?> attributeInstanceClass;

	/**
	 * the name of the movement speed attribute field in
	 * net.minecraft.server.GenericAttributes
	 */
	private String movementSpeedFieldName = null;
	private String[] movementSpeedFieldNames = { "MOVEMENT_SPEED", "d" };

	private Class<?> portableHorsesClass;
	private Method portableHorsesClassIsPortableHorseSaddleMethod;
	private Method portableHorsesClassCanUseHorseMethod;

	/**
	 * Creates an instance of a new weight manager.
	 * 
	 * @param plugin
	 *            the owning plugin
	 * @param logger
	 *            the logger errors will be logged onto
	 * @throws IllegalArgumentException
	 *             if the logger is <code>null</code>
	 */
	public WeightManager(Plugin plugin, Logger logger) {
		Validate.notNull(logger, "Logger cannot be null");
		this.plugin = plugin;
		this.logger = logger;

		this.enabledWorlds = new HashSet<>();
		this.allWorldsEnabled = false;

		this.playerArmorWeightEnabled = true;
		this.horseArmorWeightEnabled = true;
		this.horsePassengerWeightEnabled = true;
		this.enchantmentWeightEnabled = false;

		this.playerSpeedEffectEnabled = true;
		this.playerCreativeSpeedEffectEnabled = false;
		this.horseSpeedEffectEnabled = false;

		this.playerKnockbackEffectEnabled = true;

		this.portableHorsesEnabled = false;

		this.playerData = new HashMap<>();
		this.loadedPlayers = new LoadedPlayersSet();
		this.horseData = new HashMap<>();
		this.loadedHorses = new LoadedHorsesSet();
	}

	/**
	 * Initializes the manager. This will get all necessary reflection class
	 * references needed for horse speed effects. <br>
	 * The horse speed effect and PortableHorses should be disabled or enabled
	 * before calling this method.
	 * 
	 * @see #setHorseSpeedEffectEnabled(boolean)
	 * @see #setPortableHorsesEnabled(boolean)
	 */
	protected void initialize() {
		if (isHorseSpeedEffectEnabled()) {
			try {
				craftHorseClass = ReflectionHandler.getClass("CraftHorse", SubPackageType.ENTITY);
				entityHorseClass = ReflectionHandler.getClass("EntityHorse", PackageType.MINECRAFT_SERVER);
				iAttributeClass = ReflectionHandler.getClass("IAttribute", PackageType.MINECRAFT_SERVER);
				genericAttributesClass = ReflectionHandler.getClass("GenericAttributes", PackageType.MINECRAFT_SERVER);
				attributeInstanceClass = ReflectionHandler.getClass("AttributeInstance", PackageType.MINECRAFT_SERVER);
			} catch (Exception e) {
				setHorseSpeedEffectEnabled(false);
				logger.warning(
						"Failed to get NMS classes for modifing horse speeds. You are probably using the wrong version of the plugin. Install the latest version and reload the server.");
				logger.warning("The horse speed effect has been disabled.");
				logger.warning("Error log:");
				e.printStackTrace();
			}
		}
		if (isPortableHorsesEnabled()) {
			try {
				portableHorsesClass = Class.forName("com.norcode.bukkit.portablehorses.PortableHorses");
				portableHorsesClassIsPortableHorseSaddleMethod = portableHorsesClass.getMethod("isPortableHorseSaddle",
						ItemStack.class);
				portableHorsesClassCanUseHorseMethod = portableHorsesClass.getMethod("canUseHorse", Player.class,
						Horse.class);
			} catch (Exception e) {
				setPortableHorsesEnabled(false);
				logger.warning(
						"Failed to get PortableHorses classes to create compatibility with PortableHorses. You are probably using the wrong version of the plugin. Install the latest version and reload the server.");
				logger.warning(
						"It is recommended to not use PortableHorses or disable the horse speed effect. Otherwise, horse speeds may be messed up.");
				logger.warning("Error log:");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns the owning plugin.
	 * 
	 * @return the plugin
	 */
	public Plugin getPlugin() {
		return plugin;
	}

	/**
	 * Returns the logger used to print error messages.
	 * 
	 * @return The logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Returns a set of all enabled worlds with their world names. This set is
	 * backed so changes will enable or disable a world. Enable a world by adding
	 * its name into this set, disable a world by removing it.
	 * 
	 * @return a set of world names with all enabled worlds
	 */
	public Set<String> getEnabledWorlds() {
		return enabledWorlds;
	}

	/**
	 * Checks whether a world is enabled or not.
	 * 
	 * @param world
	 *            the world to check
	 * @return <code>true</code> if all worlds are enabled or this specific world is
	 *         not <code>null</code> and enabled, <code>false</code> if not
	 */
	public boolean isWorldEnabled(World world) {
		if (isAllWorldsEnabled()) {
			return true;
		}
		if (world == null) {
			return false;
		}
		return enabledWorlds.contains(world.getName());
	}

	/**
	 * Checks whether a world is enabled or not.
	 * 
	 * @param worldName
	 *            the name of the world to check
	 * @return <code>true</code>, if all worlds are enabled or this specific world
	 *         is enabled
	 */
	public boolean isWorldEnabled(String worldName) {
		if (isAllWorldsEnabled()) {
			return true;
		}
		return enabledWorlds.contains(worldName);
	}

	/**
	 * Replaces the set of enabled worlds with another one. The set should contain
	 * all names of the enabled worlds. <br>
	 * The set is backed meaning changes to it will disable or enable a world.
	 * 
	 * @param enabledWorldNames
	 *            a new set of world names
	 * @throws IllegalArgumentException
	 *             if <code>enabledWorldNames</code> is <code>null</code>
	 */
	public void setEnabledWorlds(Set<String> enabledWorldNames) {
		Validate.notNull(enabledWorldNames, "Enabled worlds cannot be null");
		this.enabledWorlds = enabledWorldNames;
	}

	/**
	 * Enables a world. This will not load all players in the world.
	 * 
	 * @param world
	 *            the world to enable
	 * @return <code>true</code> if the world has not been previously enabled
	 * @throws IllegalArgumentException
	 *             if <code>world</code> is <code>null</code>
	 */
	public boolean enableWorld(World world) {
		Validate.notNull(world, "World cannot be null");
		return enableWorld(world.getName());
	}

	/**
	 * Enables a world by its name. This will not load all players in the world.
	 * 
	 * @param worldName
	 *            the name of the world to enable
	 * @return <code>true</code> if the world has not been previously enabled
	 * @throws IllegalArgumentException
	 *             if <code>worldName</code> is <code>null</code>
	 */
	public boolean enableWorld(String worldName) {
		Validate.notNull(worldName, "World name cannot be null");
		return enabledWorlds.add(worldName);
	}

	/**
	 * Disables a world. This will not unload all players in the world.
	 * 
	 * @param world
	 *            the world to disable
	 * @return <code>true</code> if the world has not been previously disabled
	 * @throws IllegalArgumentException
	 *             if <code>world</code> is <code>null</code>
	 */
	public boolean disableWorld(World world) {
		Validate.notNull(world, "World cannot be null");
		return disableWorld(world.getName());
	}

	/**
	 * Disables a world by its name. This will not unload all players in the world.
	 * 
	 * @param worldName
	 *            the name of the world to disable
	 * @return <code>true</code> if the world has not been previously disabled
	 * @throws IllegalArgumentException
	 *             if <code>worldName</code> is <code>null</code>
	 */
	public boolean disableWorld(String worldName) {
		Validate.notNull(worldName, "World name cannot be null");
		return enabledWorlds.remove(worldName);
	}

	/**
	 * Returns <code>true</code> if all worlds are enabled. If this is the case, all
	 * worlds will be enabled even if they are not specifically enabled.
	 * 
	 * @return <code>true</code> if all worlds are enabled
	 * @see #setAllWorldsEnabled(boolean)
	 */
	public boolean isAllWorldsEnabled() {
		return allWorldsEnabled;
	}

	/**
	 * Sets all worlds enabled or not. This will not load or unload any players.
	 * 
	 * @param enabled
	 *            whether all worlds should be enabled
	 * @see #isAllWorldsEnabled()
	 */
	public void setAllWorldsEnabled(boolean enabled) {
		this.allWorldsEnabled = enabled;
	}

	/**
	 * Returns if a fix for the plugin PortableHorses is enabled. If this is the
	 * case, a compatibility problem will be addressed.
	 * 
	 * @return whether the PortableHorses fix is enabled
	 * @see #setPortableHorsesEnabled(boolean)
	 */
	public boolean isPortableHorsesEnabled() {
		return portableHorsesEnabled;
	}

	/**
	 * Sets whether a fix for the plugin PortableHorses should be applied.
	 * 
	 * @param enabled
	 *            whether the fix should be enabled
	 * @see #isPortableHorsesEnabled()
	 */
	public void setPortableHorsesEnabled(boolean enabled) {
		this.portableHorsesEnabled = enabled;
	}

	/**
	 * Returns if player weight includes the weight of his player armor.
	 * 
	 * @return <code>true</code> if player armor has a weight
	 * @see #setPlayerArmorWeightEnabled(boolean)
	 */
	public boolean isPlayerArmorWeightEnabled() {
		return playerArmorWeightEnabled;
	}

	/**
	 * Enables or disables if player weight includes the weight of his player armor.
	 * Changing this will not automatically update the weight of all players.
	 * 
	 * @param enabled
	 *            whether player armor should have a weight
	 * @see #isPlayerArmorWeightEnabled()
	 */
	public void setPlayerArmorWeightEnabled(boolean enabled) {
		this.playerArmorWeightEnabled = enabled;
	}

	/**
	 * Returns if horse weight includes the weight of its horse armor. Changing this
	 * will not automatically update the weight of all horses.
	 * 
	 * @return <code>true</code> if horse armor has a weight
	 * @see #setHorseArmorWeightEnabled(boolean)
	 */
	public boolean isHorseArmorWeightEnabled() {
		return horseArmorWeightEnabled;
	}

	/**
	 * Enables or disables if horse weight includes the weight of its horse armor.
	 * 
	 * @param enabled
	 *            whether horse armor should have a weight
	 * @see #isHorseArmorWeightEnabled()
	 */
	public void setHorseArmorWeightEnabled(boolean enabled) {
		this.horseArmorWeightEnabled = enabled;
	}

	/**
	 * Returns if horse weight includes the weight of its passenger. This does not
	 * affect the weight of the player while not on a horse.
	 * 
	 * @return <code>true</code> if passengers have a weight when riding a horse
	 * @see #setHorsePassengerWeightEnabled(boolean)
	 */
	public boolean isHorsePassengerWeightEnabled() {
		return horsePassengerWeightEnabled;
	}

	/**
	 * Enables or disables if horse weight includes the weight of its passenger.
	 * This does not affect the weight of the player while not on a horse.
	 * 
	 * @param horsePassengerWeightEnabled
	 *            whether passengers should have a weight when riding a horse
	 * @see #isHorsePassengerWeightEnabled()
	 */
	public void setHorsePassengerWeightEnabled(boolean horsePassengerWeightEnabled) {
		this.horsePassengerWeightEnabled = horsePassengerWeightEnabled;
	}

	/**
	 * Returns if the weight of armor includes the weight of its enchantments.
	 * 
	 * @return <code>true</code> if enchantments have a weight
	 * @see #setEnchantmentWeightEnabled(boolean)
	 */
	public boolean isEnchantmentWeightEnabled() {
		return enchantmentWeightEnabled;
	}

	/**
	 * Enables or disabled if the weight of armor includes the weight of its
	 * enchantments.
	 * 
	 * @param enabled
	 *            whether enchantments should have a weight
	 * @see #isEnchantmentWeightEnabled()
	 */
	public void setEnchantmentWeightEnabled(boolean enabled) {
		this.enchantmentWeightEnabled = enabled;
	}

	/**
	 * Returns if weight will effect the walk- and fly-speed of players while not in
	 * creative mode.
	 * 
	 * @return <code>true</code> if the player speed effect is enabled
	 * @see #setPlayerSpeedEffectEnabled(boolean)
	 * @see #isPlayerCreativeSpeedEffectEnabled()
	 */
	public boolean isPlayerSpeedEffectEnabled() {
		return playerSpeedEffectEnabled;
	}

	/**
	 * Enables or disables weight effecting the walk- and fly-speed of players not
	 * in creative mode.
	 * 
	 * @param enabled
	 *            whether the player speed effect should be enabled
	 * @see #isPlayerSpeedEffectEnabled()
	 * @see #setPlayerCreativeSpeedEffectEnabled(boolean)
	 */
	public void setPlayerSpeedEffectEnabled(boolean enabled) {
		this.playerSpeedEffectEnabled = enabled;
	}

	/**
	 * Returns if weight will effect the walk- and fly-speed of players while in
	 * creative mode.
	 * 
	 * @return <code>true</code> if the player speed effect is enabled in creative
	 *         mode
	 * @see #setPlayerCreativeSpeedEffectEnabled(boolean)
	 * @see #isPlayerSpeedEffectEnabled()
	 */
	public boolean isPlayerCreativeSpeedEffectEnabled() {
		return playerCreativeSpeedEffectEnabled;
	}

	/**
	 * Enables or disables weight effecting the walk- and fly-speed of players in
	 * creative mode.
	 * 
	 * @param enabled
	 *            whether the player speed effect should be enabled in creative mode
	 * @see #isPlayerCreativeSpeedEffectEnabled()
	 * @see #setPlayerSpeedEffectEnabled(boolean)
	 */
	public void setPlayerCreativeSpeedEffectEnabled(boolean enabled) {
		this.playerCreativeSpeedEffectEnabled = enabled;
	}

	/**
	 * Returns if weight will effect the movement speed of horses.
	 * 
	 * @return <code>true</code> if the horse speed effect is enabled
	 * @see #setHorseSpeedEffectEnabled(boolean)
	 */
	public boolean isHorseSpeedEffectEnabled() {
		return horseSpeedEffectEnabled;
	}

	/**
	 * Enables or disables weight effecting the movement speed of horses.
	 * 
	 * @param enabled
	 *            whether the horse speed effect should be enabled
	 * @see #isHorseSpeedEffectEnabled()
	 */
	public void setHorseSpeedEffectEnabled(boolean enabled) {
		this.horseSpeedEffectEnabled = enabled;
	}

	/**
	 * Returns an amplifier by which weight is scaled when calculating speed
	 * effects. An amplifier of <code>2.0</code> means that players and horses will
	 * move twice as slow (as the they weigh twice as much).
	 * 
	 * @return the speed effect amplifier
	 * @see #getSpeedEffectAmplifier()
	 */
	public double getSpeedEffectAmplifier() {
		return speedEffectAmplifier;
	}

	/**
	 * Changes the amplifier by which weight is scaled when calculating speed
	 * effects. An amplifier of <code>2.0</code> means that players and horses will
	 * move twice as slow (as the they weigh twice as much).
	 * 
	 * @param amplifier
	 *            the new amplifier
	 * @see #getSpeedEffectAmplifier()
	 */
	public void setSpeedEffectAmplifier(double amplifier) {
		this.speedEffectAmplifier = amplifier;
	}

	/**
	 * Returns whether weight will affect players being knocked back.
	 * 
	 * @return <code>true</code> when the knockback effect is enabled for players
	 */
	public boolean isPlayerKnockbackEffectEnabled() {
		return playerKnockbackEffectEnabled;
	}

	/**
	 * Changes whether weight will affect players being knocked back.
	 * 
	 * @param enabled
	 *            whether the player knockback effect should be enabled
	 */
	public void setPlayerKnockbackEffectEnabled(boolean enabled) {
		this.playerKnockbackEffectEnabled = enabled;
	}

	/**
	 * Returns an amplifier by which weight is scaled when calculating knockback
	 * effects. An amplifier of <code>2.0</code> means that players will be
	 * knockbacked half as much (as the they weigh twice as much).
	 * 
	 * @return the knockback effect amplifier
	 * @see #getKnockbackEffectAmplifier()
	 */
	public double getKnockbackEffectAmplifier() {
		return knockbackEffectAmplifier;
	}

	/**
	 * Changes the amplifier by which weight is scaled when calculating knockback
	 * effects. An amplifier of <code>2.0</code> means that players will be
	 * knockbacked half as much (as the they weigh twice as much).
	 * 
	 * @param amplifier
	 *            the new amplifier
	 * @see #getKnockbackEffectAmplifier()
	 */
	public void setKnockbackEffectAmplifier(double amplifier) {
		this.knockbackEffectAmplifier = amplifier;
	}

	/**
	 * Formats a weight amount to be displayed. <br>
	 * This will return a rounded value multiplied by <code>100</code>. For example,
	 * <code>formatWeight(0.975)</code> will return <code>98</code>.
	 * 
	 * @param weight
	 *            the weight amount to format
	 * @return a formatted string
	 */
	public String formatWeight(double weight) {
		return String.valueOf(Math.round(weight * 100));
	}

	/**
	 * Loads a player and creates a data container for him. Loaded players should
	 * always have the proper effects according to their current weight. When a
	 * player is loaded, their effects will be updated. If the player is riding a
	 * horse, the horse will be loaded after the player has been loaded.<br>
	 * All players need to be loaded before the weight can be updated or effects are
	 * applied. A player should be unloaded when no longer needed to avoid memory
	 * leaks.
	 * 
	 * @param player
	 *            the player to load
	 * @return <code>true</code> if the player was loaded, <code>false</code> if it
	 *         was already loaded before
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #unloadPlayer(Player)
	 * @see #updateEffects(Player)
	 * @see #loadHorse(Horse, Entity)
	 */
	public boolean loadPlayer(Player player) {
		Validate.notNull(player, "Player cannot be null");
		if (isPlayerLoaded(player)) {
			return false;
		}
		double weight = calculateWeight(player);
		PlayerData data = new PlayerData(player, weight);
		data.init();
		playerData.put(player, data);
		updateEffects(player, weight);
		if (player.getVehicle() instanceof Horse) {
			loadHorse((Horse) player.getVehicle(), player);
		}
		return true;
	}

	/**
	 * Unloads a player and removes its reference and data container. Unloaded
	 * players should have no special effects and be vanilla Minecraft players. When
	 * a player is unloaded, all their effects will be reset. If the player is
	 * riding a horse, the horse will be unloaded after the player has been
	 * unloaded. <br>
	 * The weight of unloaded players can no longer be updated and effects can no
	 * longer be applied.
	 * 
	 * @param player
	 *            the player to unload
	 * @return <code>true</code> if the player was unloaded, <code>false</code> if
	 *         it was not loaded before or <code>player</code> is <code>null</code>
	 * @see #loadPlayer(Player)
	 * @see #resetEffects(Player)
	 * @see #unloadHorse(Horse, Entity)
	 */
	public boolean unloadPlayer(Player player) {
		if (!isPlayerLoaded(player)) {
			return false;
		}
		resetEffects(player);
		playerData.get(player).destroy();
		playerData.remove(player);
		if (player.getVehicle() instanceof Horse) {
			unloadHorse((Horse) player.getVehicle(), null);
		}
		return true;
	}

	/**
	 * Checks whether a player is loaded. If the player is loaded, his weight can be
	 * updated and effects can be applied on him.
	 * 
	 * @param player
	 *            the player to check
	 * @return <code>true</code> if the player is loaded, <code>false</code> if not
	 *         or <code>player</code> is <code>null</code>
	 * @see #loadPlayer(Player)
	 * @see #unloadPlayer(Player)
	 */
	public boolean isPlayerLoaded(Player player) {
		return playerData.containsKey(player);
	}

	/**
	 * Returns a {@link Set} with all currently loaded {@link Player}s. The Set is
	 * backed and will update when a player is loaded or unloaded. Adding a player
	 * to the Set will load it, removing a player will unload it.
	 * 
	 * @return a set of loaded players
	 * @see #loadPlayer(Player)
	 * @see #unloadPlayer(Player)
	 */
	public Set<Player> getLoadedPlayers() {
		return loadedPlayers;
	}

	/**
	 * Gets the weight of a player. This value is taken from a cache and not
	 * recalculated. When the weight of the player is updated, this value gets
	 * updated.
	 * 
	 * @param player
	 *            the player to get the weight from
	 * @return a previously stored weight value or a newly calculated value if the
	 *         player is not loaded and no stored weight value is available
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #calculateWeight(Player)
	 * @see #updateWeight(Player, double)
	 */
	public double getWeight(Player player) {
		if (!isPlayerLoaded(player)) {
			return calculateWeight(player);
		}
		return playerData.get(player).getStoredWeight();
	}

	/**
	 * Calculates the weight of a player. <br>
	 * <strong>Weight = PlayerWeight + ArmorWeight</strong>
	 * 
	 * @param player
	 *            the player to calculate the weight from
	 * @return the weight of the player at the current moment
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #getPlayerWeight(Player)
	 * @see #getArmorWeight(Player)
	 */
	public double calculateWeight(Player player) {
		return getPlayerWeight(player) + getArmorWeight(player);
	}

	/**
	 * Gets the weight of an unarmored (naked) player.
	 * 
	 * @param player
	 *            the player to calculate the weight from
	 * @return the weight
	 */
	public double getPlayerWeight(Player player) {
		return PLAYER_WEIGHT;
	}

	/**
	 * Calculates the weight of the armor of a player. <br>
	 * If the player does not have the permission
	 * <code>armorweight.weight.armor</code> or player armor weight is disabled,
	 * <code>0</code> will be returned.
	 * 
	 * @param player
	 *            the player wearing the armor
	 * @return the weight of the armor at that current moment or <code>0</code> if
	 *         the player does not have valid permissions or player armor weight is
	 *         disabled
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #getArmorWeight(ItemStack[])
	 * @see #isPlayerArmorWeightEnabled()
	 */
	public double getArmorWeight(Player player) {
		Validate.notNull(player, "Player cannot be null");
		if (!player.hasPermission("armorweight.weight.armor")) {
			return 0;
		}
		return getArmorWeight(player.getInventory().getArmorContents());
	}

	/**
	 * Calculates the weight of the armor of a player without knowing the player.
	 * <br>
	 * <strong>ArmorWeight = Sum(PieceShare * PieceWeight)</strong> <br>
	 * <strong>PieceWeight = ItemWeight + EnchantmentsWeight</strong> <br>
	 * <br>
	 * If player armor weight is disabled, <code>0</code> will be returned.
	 * 
	 * @param armorContents
	 *            all <code>ItemStack</code>s worn as armor in order as the
	 *            {@link ArmorPart} IDs
	 * @return the weight of the armor or <code>0</code>, if player armor weight is
	 *         not enabled.
	 * @throws IllegalArgumentException
	 *             if <code>armorContents</code> is <code>null</code>
	 * @see #getArmorWeight(Player)
	 * @see #isPlayerArmorWeightEnabled()
	 * @see ArmorPart
	 */
	public double getArmorWeight(ItemStack[] armorContents) {
		Validate.notNull(armorContents, "Armor contents cannot be null");
		if (!isPlayerArmorWeightEnabled()) {
			return 0D;
		}
		double weight = 0D;
		for (int i = 0; i < armorContents.length; i++) {
			ItemStack stack = armorContents[i];
			ArmorPart piece = ArmorPart.valueOf(i);
			weight += (ArmorType.getItemWeight(stack) + getEnchantmentsWeight(stack)) * piece.getWeightShare();
		}
		return weight;
	}

	/**
	 * Updates the stored weight value of a player with the player's current weight.
	 * If that value is different from the previous stored old value, the effects of
	 * the player will be updated and a {@link PlayerWeightChangeEvent} is called.
	 * <br>
	 * If the player is not loaded, this method will not update the weight.
	 * 
	 * @param player
	 *            the player to update
	 * @return <code>true</code> if the weight of the player has changed
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #getWeight(Player)
	 * @see #updateEffects(Player)
	 * @see #calculateWeight(Player)
	 * @see PlayerWeightChangeEvent
	 */
	public boolean updateWeight(Player player) {
		return updateWeight(player, calculateWeight(player));
	}

	/**
	 * Updates the stored weight value of a player. If that value is different from
	 * the previous stored old value, the effects of the player will be updated and
	 * a {@link PlayerWeightChangeEvent} is called. If the player is not loaded,
	 * <code>false</code> will be returned.
	 * 
	 * @param player
	 *            the player to update
	 * @param weight
	 *            the current weight of the player
	 * @return <code>true</code> if the weight of the player has changed
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #getWeight(Player)
	 * @see #updateEffects(Player, double)
	 * @see PlayerWeightChangeEvent
	 */
	public boolean updateWeight(Player player, double weight) {
		Validate.notNull(player, "Player cannot be null");
		if (!isPlayerLoaded(player)) {
			return false;
		}
		if (playerData.get(player).updateWeight(weight)) {
			updateEffects(player, weight);
			return true;
		}
		return false;
	}

	/**
	 * Updates the stored weight value of a player with the player's current weight
	 * on the next server tick. This is useful for events that haven't updated
	 * player properties yet. The weight will be calculated next server tick. <br>
	 * If the calculated weight value is different from the previous stored old
	 * value, the effects of the player will be updated and a
	 * {@link PlayerWeightChangeEvent} is called. <br>
	 * If the player is not loaded, this method will not update the weight.
	 * 
	 * @param player
	 *            the player to update
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #updateWeight(Player)
	 * @see #updateWeightLater(Player, boolean)
	 * @see #getWeight(Player)
	 * @see #updateEffects(Player)
	 * @see #calculateWeight(Player)
	 * @see PlayerWeightChangeEvent
	 */
	public void updateWeightLater(final Player player) {
		updateWeightLater(player, false);
	}

	/**
	 * Updates the stored weight value of a player with the player's current weight
	 * on the next server tick. This is useful for events that haven't updated
	 * player properties yet. The weight will be calculated next server tick. <br>
	 * If the calculated weight value is different from the previous stored old
	 * value, a {@link PlayerWeightChangeEvent} is called. If
	 * <code>forceUpdateEffects</code> is enabled, or if the weight changed, the
	 * effects are updated afterwards.<br>
	 * If the player is not loaded, this method will not update the weight.
	 * 
	 * @param player
	 *            the player to update
	 * @param forceUpdateEffects
	 *            <code>true</code> if {@link #updateEffects(Player)} should be
	 *            called even if the weight is not changed
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #updateWeight(Player)
	 * @see #updateWeightLater(Player)
	 * @see #getWeight(Player)
	 * @see #updateEffects(Player)
	 * @see #calculateWeight(Player)
	 * @see PlayerWeightChangeEvent
	 */
	public void updateWeightLater(final Player player, final boolean forceUpdateEffects) {
		Validate.notNull(player, "Player cannot be null");
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				if (!updateWeight(player) && forceUpdateEffects) {
					updateEffects(player);
				}
			}
		});
	}

	/**
	 * Updates the effects of a player according to his current weight. If the
	 * player is not loaded, no effects will be applied and <code>false</code> will
	 * be returned.
	 * <p>
	 * This currently includes the following effects:
	 * <ul>
	 * <li><strong>Movement Speed:</strong> Changes the walk- and fly-speed of
	 * players. Requires this feature to be enabled and the player to have the
	 * permission <code>armorweight.effect.speed</code>.</li>
	 * </ul>
	 * 
	 * @param player
	 *            the player
	 * @return <code>true</code> if the effects of the player have been updated,
	 *         <code>false</code> if not or <code>player</code> was
	 *         <code>null</code>
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #updateEffects(Player, double)
	 * @see #getWeight(Player)
	 */
	public boolean updateEffects(Player player) {
		return updateEffects(player, getWeight(player));
	}

	/**
	 * Updates the effects of a player according to a weight level. If the player is
	 * not loaded, no effects will be applied and <code>false</code> will be
	 * returned.
	 * <p>
	 * This currently includes the following effects:
	 * <ul>
	 * <li><strong>Movement Speed:</strong> Changes the walk- and fly-speed of
	 * players. Requires this feature to be enabled and the player to have the
	 * permission <code>armorweight.effect.speed</code>.</li>
	 * </ul>
	 * 
	 * @param player
	 *            the player
	 * @param weight
	 *            the weight determining the level of the effects
	 * @return <code>true</code> if the effects of the player have been updated,
	 *         <code>false</code> if not or <code>player</code> was
	 *         <code>null</code>
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 */
	public boolean updateEffects(Player player, double weight) {
		Validate.notNull(player, "Player cannot be null");
		if (!isPlayerLoaded(player)) {
			return false;
		}
		boolean speedEffect = player.hasPermission("armorweight.effect.speed");
		speedEffect = speedEffect && (player.getGameMode() != GameMode.CREATIVE ? isPlayerSpeedEffectEnabled()
				: isPlayerCreativeSpeedEffectEnabled());
		// if player has had speed effect but no longer has, it has to be reset
		if (playerData.get(player).hadSpeedEffect() != speedEffect) {
			if (!speedEffect) {
				player.setWalkSpeed(DEFAULT_WALK_SPEED);
				player.setFlySpeed(DEFAULT_FLY_SPEED);
			}
			playerData.get(player).setHadSpeedEffect(speedEffect);
		}
		if (speedEffect) {
			player.setWalkSpeed(getPlayerSpeed(weight, DEFAULT_WALK_SPEED));
			player.setFlySpeed(getPlayerSpeed(weight, DEFAULT_FLY_SPEED));
		}
		playerData.get(player).setDirty(false);
		return true;
	}

	/**
	 * @deprecated Misleading method name. <strong>Use {@link #resetEffects(Player)}
	 *             instead.</strong>
	 * @param player
	 *            the player
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #resetEffects(Player)
	 */
	@Deprecated
	public void resetWeight(Player player) {
		resetEffects(player);
	}

	/**
	 * Resets the effects a player by updating the effects to the default weight of
	 * the player. After this, a player should be just like a normal, vanilla
	 * Minecraft player.
	 * 
	 * @deprecated Players should be unloaded instead.
	 * @param player
	 *            the player to reset
	 * @throws IllegalArgumentException
	 *             if <code>player</code> is <code>null</code>
	 * @see #updateEffects(Player, double)
	 * @see #DEFAULT_PLAYER_WEIGHT
	 * @see #unloadPlayer(Player)
	 */
	@Deprecated
	public void resetEffects(Player player) {
		updateEffects(player, DEFAULT_PLAYER_WEIGHT);
	}

	/**
	 * Modifies a velocity {@link Vector} by scaling it according to a player's
	 * weight. This changes the given Vector and does not create a new one. If the
	 * player is not loaded or has insufficient permission to be affected by
	 * knockback, or the knockback effect is not enabled, this method will not
	 * change the given <code>knockbackVelocity</code>.
	 * 
	 * @param player
	 *            the player who's weight is to be used
	 * @param knockbackVelocity
	 *            the knockback velocity
	 * @return the same knockback velocity
	 * @throws NullPointerException
	 *             if the <code>player</code> or the <code>knockbackVelocity</code>
	 *             is <code>null</code>
	 * @see #updateKnockbackEffect(Player, Vector, double)
	 */
	public Vector updateKnockbackEffect(Player player, Vector knockbackVelocity) {
		return updateKnockbackEffect(player, knockbackVelocity, getWeight(player));
	}

	/**
	 * Modifies a velocity {@link Vector} by scaling it according to a given weight
	 * (which does not necessarily have to be of that player). This changes the
	 * given Vector and does not create a new one. If the player is not loaded or
	 * has insufficient permission to be affected by knockback, or the knockback
	 * effect is not enabled, this method will not change the given
	 * <code>knockbackVelocity</code>.
	 * 
	 * @param player
	 *            the player who's weight is to be used
	 * @param knockbackVelocity
	 *            the knockback velocity
	 * @param weight
	 *            the weight used to calculate how knockback should be affected
	 * @return the same knockback velocity
	 * @throws NullPointerException
	 *             if the <code>player</code> or the <code>knockbackVelocity</code>
	 *             is <code>null</code>
	 * @see #updateKnockbackEffect(Player, Vector)
	 */
	public Vector updateKnockbackEffect(Player player, Vector knockbackVelocity, double weight) {
		Validate.notNull(player, "Player cannot be null");
		Validate.notNull(knockbackVelocity, "Knockback velocity cannot be null");
		if (!isPlayerLoaded(player)) {
			return knockbackVelocity;
		}
		boolean speedEffect = player.hasPermission("armorweight.effect.knockback");
		speedEffect = speedEffect && player.getGameMode() != GameMode.CREATIVE && isPlayerKnockbackEffectEnabled();
		if (speedEffect) {
			float velocityFactor = getPlayerKnockback(getWeight(player));
			knockbackVelocity.multiply(velocityFactor);
		}
		return knockbackVelocity;
	}

	/**
	 * Returns the speed of a player at a certain weight.
	 * <p>
	 * This method takes care of preventing {@link IllegalArgumentException}s when
	 * calling {@link Player#setWalkSpeed(float)} or
	 * {@link Player#setFlySpeed(float)} by limiting the speed to a value between
	 * <code>-1F</code> and <code>1F</code>.
	 * 
	 * @param weight
	 *            the weight of the player
	 * @param defaultSpeed
	 *            the speed a player with default weight would have
	 * @return the speed of a player having the specified weight, ranges from
	 *         <code>-1F</code> to <code>1F</code>
	 */
	public float getPlayerSpeed(double weight, double defaultSpeed) {
		double amplifier = getSpeedEffectAmplifier();
		float speed = (float) (DEFAULT_PLAYER_WEIGHT / (amplifier * weight) * defaultSpeed);
		if (speed > 1F) {
			speed = 1F;
		}
		if (speed < -1F) {
			speed = -1F;
		}
		return speed;
	}

	/**
	 * Calculates the factor by which knockback is scaled at a given weight.
	 * 
	 * @param weight
	 *            the weight at which knockback is to be calculated
	 * @return a scalar for a knockback velocity {@link Vector}
	 */
	public float getPlayerKnockback(double weight) {
		double amplifier = getKnockbackEffectAmplifier();
		float factor = (float) (DEFAULT_PLAYER_WEIGHT / (amplifier * weight));
		if (factor < 0F) {
			factor = 0F;
		}
		return factor;
	}

	/**
	 * Gets the weight of one level of that enchantment. The weight is taken from
	 * the enchantment weight map, or if that does not specify a value, the default
	 * value is used.
	 * 
	 * @param enchantment
	 *            the enchantment type
	 * @return the weight each level of that enchantment type has, or the default
	 *         value if the enchantment has no specified weight or is
	 *         <code>null</code>
	 */
	public double getWeight(Enchantment enchantment) {
		Double d = ENCHANTMENT_WEIGHTS.containsKey(enchantment) ? ENCHANTMENT_WEIGHTS.get(enchantment)
				: DEFAULT_ENCHANTMENT_WEIGHT;
		return d == null ? 0 : d;
	}

	/**
	 * Calculates the weight a specific level of an enchantment type has.
	 * 
	 * @param enchantment
	 *            the enchantment type
	 * @param level
	 *            the level of the enchantment
	 * @return the weight the level of the enchantment has
	 * @throws IllegalArgumentException
	 *             if <code>level</code> is less then <code>0</code>
	 */
	public double getWeight(Enchantment enchantment, int level) {
		Validate.isTrue(level >= 0, "Enchantment level has to be greater or equal than 0 (" + level + " < 0)");
		return getWeight(enchantment) * level;
	}

	/**
	 * Calculates the weight the enchantments on an <code>ItemStack</code> have. If
	 * enchantment weight is disabled, <code>0</code> will be returned.
	 * 
	 * @param item
	 *            the <code>ItemStack</code>
	 * @return the weight of all enchantments on the item or <code>0</code>, if
	 *         enchantment weight is disabled or the item is <code>null</code>.
	 */
	public double getEnchantmentsWeight(ItemStack item) {
		if (!isEnchantmentWeightEnabled()) {
			return 0D;
		}
		if (item == null) {
			return 0D;
		}
		double weight = 0D;
		for (Entry<Enchantment, Integer> entry : item.getEnchantments().entrySet()) {
			weight += getWeight(entry.getKey(), entry.getValue());
		}
		return weight;
	}

	/**
	 * Loads a horse and creates a data container for it. Loaded horses should
	 * always have the proper effects according to their current weight. When a
	 * horse is loaded, the current horse speed is stored and used as default speed.
	 * After that, the effects of the horse will be updated according to its weight.
	 * <br>
	 * All horses need to be loaded before the weight can be updated or effects are
	 * applied. A horse should be unloaded when no longer needed to avoid memory
	 * leaks. <br>
	 * If an error occurs while getting the horse speed, the horse will not be
	 * loaded and an error will be printed to the logger.
	 * 
	 * @param horse
	 *            the horse to load
	 * @return <code>true</code> if the horse was successfully loaded,
	 *         <code>false</code> if it had already been loaded or an error occurred
	 *         while getting the horse speed
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #loadHorse(Horse, Entity)
	 * @see #unloadHorse(Horse)
	 * @see #updateEffects(Horse)
	 */
	@SuppressWarnings("deprecation")
	public boolean loadHorse(Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		return loadHorse(horse, horse.getPassenger());
	}

	/**
	 * Loads a horse and creates a data container for it. Loaded horses should
	 * always have the proper effects according to their current weight. When a
	 * horse is loaded, the current horse speed is stored and used as default speed.
	 * After that, the effects of the horse will be updated according to its weight.
	 * <br>
	 * All horses need to be loaded before the weight can be updated or effects are
	 * applied. A horse should be unloaded when no longer needed to avoid memory
	 * leaks. <br>
	 * If an error occurs while getting the horse speed, the horse will not be
	 * loaded and an error will be printed to the logger.
	 * 
	 * @param horse
	 *            the horse to load
	 * @param passenger
	 *            the passenger of the horse or <code>null</code> if there is no
	 *            passenger
	 * @return <code>true</code> if the horse was successfully loaded,
	 *         <code>false</code> if it had already been loaded or an error occurred
	 *         while getting the horse speed
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #unloadHorse(Horse, Entity)
	 * @see #updateEffects(Horse, Entity)
	 */
	public boolean loadHorse(final Horse horse, final Entity passenger) {
		Validate.notNull(horse, "Horse cannot be null");
		if (isHorseLoaded(horse)) {
			return false;
		}
		try {
			double weight = calculateWeight(horse, passenger);
			HorseData data = new HorseData(horse, weight);
			if (isHorseSpeedEffectEnabled()) {
				data.setDefaultSpeed(getHorseSpeed(horse));
			}
			horseData.put(horse, data);
			updateEffects(horse, weight);
			return true;
		} catch (Exception e) {
			logger.warning("Failed to load horse " + horse + " and get its speed (probaly an NMS issue):");
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Unloads a horse and removes its reference and data container. Unloaded horses
	 * should have no special effects and have their default speed. When a horse is
	 * unloaded, all their effects will be reset. <br>
	 * The weight of unloaded horses can no longer be updated and effects can no
	 * longer be applied.
	 * 
	 * @param horse
	 *            the horse to unload
	 * @return <code>true</code> if the horse was unloaded, <code>false</code> if it
	 *         was not loaded before, an error occurred while reseting the effects
	 *         or <code>horse</code> is <code>null</code>
	 * @see #loadHorse(Horse)
	 */
	@SuppressWarnings("deprecation")
	public boolean unloadHorse(Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		return unloadHorse(horse, horse.getPassenger());
	}

	/**
	 * Unloads a horse and removes its reference and data container. Unloaded horses
	 * should have no special effects and have their default speed. When a horse is
	 * unloaded, all their effects will be reset. <br>
	 * The weight of unloaded horses can no longer be updated and effects can no
	 * longer be applied.
	 * 
	 * @param horse
	 *            the horse to unload
	 * @param passenger
	 *            the passenger of the horse or <code>null</code> if there is no
	 *            passenger
	 * @return <code>true</code> if the horse was unloaded, <code>false</code> if it
	 *         was not loaded before, an error occurred while reseting the effects
	 *         or <code>horse</code> is <code>null</code>
	 * @see #loadHorse(Horse, Entity)
	 */
	public boolean unloadHorse(Horse horse, Entity passenger) {
		if (!isHorseLoaded(horse)) {
			return false;
		}
		if (!resetEffects(horse)) {
			return false;
		}
		horseData.remove(horse);
		return true;
	}

	/**
	 * Checks whether a horse is loaded. If the horse is loaded, its weight can be
	 * updated and effects can be applied on it.
	 * 
	 * @param horse
	 *            the horse to check
	 * @return <code>true</code> if the horse loaded, <code>false</code> if not or
	 *         the <code>horse</code> is <code>null</code>
	 * @see #loadHorse(Horse, Entity)
	 * @see #unloadHorse(Horse, Entity)
	 */
	public boolean isHorseLoaded(Horse horse) {
		return horseData.containsKey(horse);
	}

	/**
	 * Returns a {@link Set} with all currently loaded {@link Horse}s. The Set is
	 * backed and will update when a horse is loaded or unloaded. Adding a horse to
	 * the Set will load it, removing a horse will unload it.
	 * 
	 * @return a set of loaded horses
	 * @see #loadHorse(Horse)
	 * @see #unloadHorse(Horse)
	 */
	public Set<Horse> getLoadedHorses() {
		return loadedHorses;
	}

	/**
	 * Gets the weight of a horse. This value is taken from a cache and not
	 * recalculated. When the weight of the horse is updated, this value gets
	 * updated.
	 * 
	 * @param horse
	 *            the horse to get the weight from
	 * @return a previously stored weight value or a newly calculated value if the
	 *         horse is not loaded and no stored weight value is available
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #calculateWeight(Horse)
	 * @see #updateWeight(Horse)
	 */
	@SuppressWarnings("deprecation")
	public double getWeight(Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		return getWeight(horse, horse.getPassenger());
	}

	/**
	 * Gets the weight of a horse. This value is taken from a cache and not
	 * recalculated. When the weight of the horse is updated, this value gets
	 * updated.
	 * 
	 * @param horse
	 *            the horse to get the weight from
	 * @param passenger
	 *            the passenger riding the horse or <code>null</code> if there is no
	 *            passenger; is used if weight has to be calculated if horse is not
	 *            loaded
	 * @return a previously stored weight value or a newly calculated value if the
	 *         horse is not loaded and no stored weight value is available
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #calculateWeight(Horse, Entity)
	 * @see #updateWeight(Horse, double)
	 */
	public double getWeight(Horse horse, Entity passenger) {
		if (!isHorseLoaded(horse)) {
			return calculateWeight(horse, passenger);
		}
		return horseData.get(horse).getStoredWeight();
	}

	/**
	 * Calculates the weight of a horse. <br>
	 * <strong>Weight = HorseWeight + PassengerWeight + ArmorWeight</strong>
	 * 
	 * @param horse
	 *            the horse to calculate the weight from
	 * @return the weight of the horse at the current moment
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #calculateWeight(Horse, Entity)
	 */
	@SuppressWarnings("deprecation")
	public double calculateWeight(Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		return calculateWeight(horse, horse.getPassenger());
	}

	/**
	 * Calculates the weight of a horse. <br>
	 * <strong>Weight = HorseWeight + PassengerWeight + ArmorWeight</strong>
	 * 
	 * @param horse
	 *            the horse to calculate the weight from
	 * @param passenger
	 *            the passenger riding the horse or <code>null</code> if there is no
	 *            passenger
	 * @return the weight of the horse at the current moment
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #getHorseWeight(Horse)
	 * @see #getArmorWeight(Horse)
	 */
	public double calculateWeight(Horse horse, Entity passenger) {
		Validate.notNull(horse, "Horse cannot be null");

		double weight = getHorseWeight(horse);
		if (isHorsePassengerWeightEnabled() && passenger instanceof Player) {
			weight += getWeight((Player) passenger);
		}
		weight += getArmorWeight(horse.getInventory());
		return weight;
	}

	/**
	 * Gets the weight of an unarmored (naked) horse without a passenger.
	 * 
	 * @param horse
	 *            the horse to calculate the weight from
	 * @return the weight
	 */
	public double getHorseWeight(Horse horse) {
		return HORSE_WEIGHT;
	}

	/**
	 * Calculates the weight of the armor of a horse.
	 * 
	 * @param horse
	 *            the horse wearing the armor
	 * @return the weight of the armor at that current moment or <code>0</code> if
	 *         horse armor weight is disabled
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #getArmorWeight(HorseInventory)
	 * @see #isHorseArmorWeightEnabled()
	 */
	public double getArmorWeight(Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		return getArmorWeight(horse.getInventory());
	}

	/**
	 * Calculates the weight of the armor of a horse with its inventory. <br>
	 * <strong>Weight = ItemWeight + EnchantmentsWeight</strong> <br>
	 * <br>
	 * If horse armor weight is disabled, <code>0</code> will be returned.
	 * 
	 * @param inventory
	 *            the horse inventory containing the armor
	 * @return the weight of the armor or <code>0</code>, if horse armor weight is
	 *         not enabled.
	 * @throws IllegalArgumentException
	 *             if <code>inventory</code> is <code>null</code>
	 * @see #getArmorWeight(Horse)
	 * @see #isHorseArmorWeightEnabled()
	 */
	public double getArmorWeight(HorseInventory inventory) {
		Validate.notNull(inventory, "Horse inventory cannot be null");
		if (!isHorseArmorWeightEnabled()) {
			return 0D;
		}
		ItemStack armor = inventory.getArmor();
		if (armor == null) {
			return 0D;
		}
		double weight = ArmorType.getItemWeight(armor);
		weight += getEnchantmentsWeight(armor);

		return weight;
	}

	/**
	 * Updates the stored weight value of a horse with the horse's current weight.
	 * If that value is different from the previous stored old value, the effects of
	 * the horse will be updated and a {@link HorseWeightChangeEvent} is called. If
	 * the horse is not loaded, this method will not change the weight.
	 * 
	 * @param horse
	 *            the horse to update
	 * @return <code>true</code> if the weight of the horse has changed
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #calculateWeight(Horse)
	 * @see #updateEffects(Horse, double)
	 * @see HorseWeightChangeEvent
	 */
	public boolean updateWeight(Horse horse) {
		return updateWeight(horse, calculateWeight(horse));
	}

	/**
	 * Updates the stored weight value of a horse with the horse's current weight.
	 * If that value is different from the previous stored old value, the effects of
	 * the horse will be updated and a {@link HorseWeightChangeEvent} is called. If
	 * the horse is not loaded, this method will not change the weight.
	 * 
	 * @param horse
	 *            the horse to update
	 * @param passenger
	 *            the passenger riding the horse or <code>null</code> if there is no
	 *            passenger
	 * @return <code>true</code> if the weight of the horse has changed
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #calculateWeight(Horse, Entity)
	 * @see #updateEffects(Horse, double)
	 * @see HorseWeightChangeEvent
	 */
	public boolean updateWeight(Horse horse, Entity passenger) {
		return updateWeight(horse, calculateWeight(horse, passenger));
	}

	/**
	 * Updates the stored weight value of a horse. If that value is different from
	 * the previous stored old value, the effects of the horse will be updated and a
	 * {@link HorseWeightChangeEvent} is called. If the horse is not loaded, this
	 * method will not change the weight.
	 * 
	 * @param horse
	 *            the horse to update
	 * @param weight
	 *            the current weight of the horse
	 * @return <code>true</code> if the weight of the horse has changed
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #updateEffects(Horse, double)
	 * @see HorseWeightChangeEvent
	 */
	public boolean updateWeight(Horse horse, double weight) {
		Validate.notNull(horse, "Horse cannot be null");
		if (!isHorseLoaded(horse)) {
			return false;
		}
		if (horseData.get(horse).updateWeight(weight)) {
			updateEffects(horse, weight);
			return true;
		}
		return false;
	}

	/**
	 * Updates the stored weight value of a horse with the horse's current weight on
	 * the next server tick. This is useful for events that haven't updated horse
	 * properties yet. The weight and the passenger will be calculated next server
	 * tick. <br>
	 * If the calculated weight value is different from the previous stored old
	 * value, the effects of the horse will be updated and a
	 * {@link HorseWeightChangeEvent} is called. If the horse is not loaded, this
	 * method will not change the weight.
	 * 
	 * @param horse
	 *            the horse to update
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #updateWeight(Horse)
	 * @see #calculateWeight(Horse)
	 * @see #updateEffects(Horse, double)
	 * @see HorseWeightChangeEvent
	 */
	public void updateWeightLater(final Horse horse) {
		Validate.notNull(horse, "Horse cannot be null");
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				updateWeight(horse);
			}
		});
	}

	/**
	 * Updates the stored weight value of a horse with the horse's current weight on
	 * the next server tick. This is useful for events that haven't updated horse
	 * properties yet. The weight will be calculated next server tick. <br>
	 * If the calculated weight value is different from the previous stored old
	 * value, the effects of the horse will be updated and a
	 * {@link HorseWeightChangeEvent} is called. If the horse is not loaded, this
	 * method will not change the weight.
	 * 
	 * @param horse
	 *            the horse to update
	 * @param passenger
	 *            the passenger riding the horse or <code>null</code> if there is no
	 *            passenger
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #updateWeight(Horse)
	 * @see #calculateWeight(Horse)
	 * @see #updateEffects(Horse, double)
	 * @see HorseWeightChangeEvent
	 */
	public void updateWeightLater(final Horse horse, final Entity passenger) {
		Validate.notNull(horse, "Horse cannot be null");
		plugin.getServer().getScheduler().runTask(plugin, new Runnable() {
			@Override
			public void run() {
				updateWeight(horse, passenger);
			}
		});
	}

	/**
	 * Updates the effects of a horse according to a its current weight. <br>
	 * If an error occurs while changing the horse speed, the error will be logged
	 * and <code>false</code> will be returned.
	 * <p>
	 * This currently includes the following effects:
	 * <ul>
	 * <li><strong>Movement Speed:</strong> Changes the speed of horses. Requires
	 * this feature to be enabled.</li>
	 * </ul>
	 * 
	 * @param horse
	 *            the horse
	 * @return <code>true</code> if the effects have been updated successfully,
	 *         <code>false</code> if an error has occurred
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #getWeight(Horse)
	 * @see #updateEffects(Horse, double)
	 */
	public boolean updateEffects(Horse horse) {
		return updateEffects(horse, getWeight(horse));
	}

	/**
	 * Updates the effects of a horse according to a its current weight. <br>
	 * If an error occurs while changing the horse speed, the error will be logged
	 * and <code>false</code> will be returned.
	 * <p>
	 * This currently includes the following effects:
	 * <ul>
	 * <li><strong>Movement Speed:</strong> Changes the speed of horses. Requires
	 * this feature to be enabled.</li>
	 * </ul>
	 * 
	 * @param horse
	 *            the horse
	 * @param passenger
	 *            the passenger riding the horse or <code>null</code> if there is no
	 *            passenger
	 * @return <code>true</code> if the effects have been updated successfully,
	 *         <code>false</code> if an error has occurred
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #getWeight(Horse, Entity)
	 * @see #updateEffects(Horse, double)
	 */
	public boolean updateEffects(Horse horse, Entity passenger) {
		return updateEffects(horse, getWeight(horse, passenger));
	}

	/**
	 * Updates the effects of a horse according to a weight level. <br>
	 * If an error occurs while changing the horse speed, the error will be logged
	 * and <code>false</code> will be returned.
	 * <p>
	 * This currently includes the following effects:
	 * <ul>
	 * <li><strong>Movement Speed:</strong> Changes the speed of horses. Requires
	 * this feature to be enabled.</li>
	 * </ul>
	 * 
	 * @param horse
	 *            the horse
	 * @param weight
	 *            the weight determining the level of the effects
	 * @return <code>true</code> if the effects have been updated successfully,
	 *         <code>false</code> if an error has occurred
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 */
	public boolean updateEffects(Horse horse, double weight) {
		Validate.notNull(horse, "Horse cannot be null");
		if (!isHorseLoaded(horse)) {
			return false;
		}

		if (isHorseSpeedEffectEnabled()) {
			try {
				setHorseSpeed(horse, getHorseSpeed(weight, getDefaultHorseSpeed(horse)));
			} catch (Exception e) {
				logger.warning("Failed to update speed of horse " + horse + " (probaly an NMS issue):");
				e.printStackTrace();
				return false;
			}
		}

		horseData.get(horse).setDirty(false);
		return true;
	}

	/**
	 * Resets the effects of a horse by updating the effects to the default weight
	 * of the player. After this, a horse should be just like a normal, vanilla
	 * Minecraft horse with its default speed.
	 * 
	 * @deprecated Horses should be unloaded instead.
	 * @param horse
	 *            the horse to reset
	 * @return <code>true</code> if the effects have been updated successfully,
	 *         <code>false</code> if an error has occurred
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #updateEffects(Horse, double)
	 * @see #DEFAULT_HORSE_WEIGHT
	 * @see #unloadHorse(Horse, Entity)
	 */
	@Deprecated
	public boolean resetWeight(Horse horse) {
		return resetEffects(horse);
	}

	/**
	 * Resets the effects of a horse by updating the effects to the default weight
	 * of the player. After this, a horse should be just like a normal, vanilla
	 * Minecraft horse with its default speed.
	 * 
	 * @deprecated Horses should be unloaded instead.
	 * @param horse
	 *            the horse to reset
	 * @return <code>true</code> if the effects have been updated successfully,
	 *         <code>false</code> if an error has occurred
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 * @see #updateEffects(Horse, double)
	 * @see #DEFAULT_HORSE_WEIGHT
	 * @see #unloadHorse(Horse, Entity)
	 */
	public boolean resetEffects(Horse horse) {
		return updateEffects(horse, DEFAULT_HORSE_WEIGHT);
	}

	/**
	 * Returns the speed of a horse at a certain weight.
	 * <p>
	 * This method limits the speed to a value between <code>-1</code> and
	 * <code>1</code>.
	 * 
	 * @param weight
	 *            the weight of the horse
	 * @param defaultSpeed
	 *            the speed the horse with default weight would have
	 * @return the speed of a horse having the specified weight, ranges from
	 *         <code>-1</code> to <code>1</code>
	 */
	public double getHorseSpeed(double weight, double defaultSpeed) {
		double amplifier = getSpeedEffectAmplifier();
		double speed = DEFAULT_HORSE_WEIGHT / (amplifier * weight) * defaultSpeed;
		if (speed > 1F) {
			speed = 1F;
		}
		if (speed < -1F) {
			speed = -1F;
		}
		return speed;
	}

	/**
	 * Gets the speed a horse has while having default weight.
	 * 
	 * @param horse
	 *            the horse to get the default speed from
	 * @return the default speed of the horse
	 * @throws IllegalStateException
	 *             if the horse is not loaded
	 * @throws IllegalArgumentException
	 *             if <code>horse</code> is <code>null</code>
	 */
	public double getDefaultHorseSpeed(Horse horse) throws IllegalStateException {
		Validate.notNull(horse, "Horse cannot be null");
		if (!isHorseLoaded(horse)) {
			throw new IllegalStateException("Horse has to be loaded");
		}
		return horseData.get(horse).getDefaultSpeed();
	}

	protected synchronized double getHorseSpeed(Horse horse) throws Exception {
		Validate.notNull(horse, "Horse cannot be null");

		Object craftHorse = craftHorseClass.cast(horse);
		Object entityHorse = craftHorseClass.getMethod("getHandle").invoke(craftHorse);

		Object genericAttribute = genericAttributesClass.getField(getMovementSpeedFieldName()).get(null);
		Object attributeInstance = entityHorseClass.getMethod("getAttributeInstance", iAttributeClass)
				.invoke(entityHorse, genericAttribute);

		return (double) attributeInstanceClass.getMethod("getValue").invoke(attributeInstance);
	}

	protected synchronized void setHorseSpeed(Horse horse, double speed) throws Exception {
		Validate.notNull(horse, "Horse cannot be null");

		Object craftHorse = craftHorseClass.cast(horse);
		Object entityHorse = craftHorseClass.getMethod("getHandle").invoke(craftHorse);

		Object genericAttribute = genericAttributesClass.getField(getMovementSpeedFieldName()).get(null);
		Object attributeInstance = entityHorseClass.getMethod("getAttributeInstance", iAttributeClass)
				.invoke(entityHorse, genericAttribute);

		attributeInstanceClass.getMethod("setValue", Double.TYPE).invoke(attributeInstance, speed);
	}

	private String getMovementSpeedFieldName() throws Exception {
		if (movementSpeedFieldName == null) {
			for (String fieldName : movementSpeedFieldNames) {
				try {
					genericAttributesClass.getField(fieldName);

					// field exists
					movementSpeedFieldName = fieldName;
					return fieldName;
				} catch (NoSuchFieldException e) {
				}
			}

			// no field name works
			throw new Exception("Generic movement speed attribute could not be found. Tried "
					+ Arrays.toString(movementSpeedFieldNames) + ", but none of them worked.");
		}
		return movementSpeedFieldName;
	}

	protected boolean isKickedOffHorse(Player player, Horse horse, ItemStack saddle) throws Exception {
		Plugin ph = plugin.getServer().getPluginManager().getPlugin("PortableHorses");
		return (boolean) portableHorsesClassIsPortableHorseSaddleMethod.invoke(ph, saddle)
				&& (boolean) portableHorsesClassCanUseHorseMethod.invoke(ph, player, horse);
	}
}
