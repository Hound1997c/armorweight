package com.zettelnet.armorweight;

import java.util.logging.Logger;

import org.bstats.bukkit.Metrics;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * This program is licensed under Attribution-NonCommercial-NoDerivatives 4.0
 * International. You are free to share this program under the term of
 * attribution. You may not use this program for commercial purposes and may not
 * create derivatives. To view the full license, check the included license.txt
 * file.
 * 
 * @author Zettelkasten
 * 
 */
public class ArmorWeightPlugin extends JavaPlugin {

	private Logger log;

	private ArmorWeightConfiguration config;
	private ArmorWeightLanguage lang;
	private ArmorWeightCommands commands;

	private WeightManager weightManager;
	private WeightListener listener;

	private WeightTracker tracker;

	public ArmorWeightPlugin() {
	}

	@Override
	public void onEnable() {
		log = getLogger();
		weightManager = new WeightManager(this, log);

		config = new ArmorWeightConfiguration(this, "config.yml", "config.yml", weightManager);
		config.load();
		lang = new ArmorWeightLanguage(this, "lang.yml", "lang.yml");
		lang.loadDefaults();
		lang.load();

		weightManager.setPortableHorsesEnabled(isPortableHorsesFound());
		weightManager.initialize();

		listener = new WeightListener(weightManager);
		this.getServer().getPluginManager().registerEvents(listener, this);

		this.commands = new ArmorWeightCommands(this);

		this.tracker = new WeightTracker(this, weightManager);
		this.tracker.register();

		if (config.metricsEnabled()) {
			new Metrics(this);
		}

		for (Player player : getServer().getOnlinePlayers()) {
			if (weightManager.isWorldEnabled(player.getWorld())) {
				weightManager.loadPlayer(player);
			}
		}

		log.info("Enabled successfully.");
	}

	private boolean isPortableHorsesFound() {
		return getServer().getPluginManager().getPlugin("PortableHorses") != null;
	}

	@Override
	public void onDisable() {
		HandlerList.unregisterAll(this);

		for (Player player : getServer().getOnlinePlayers()) {
			weightManager.unloadPlayer(player);
		}

		log.info("Disabled successfully.");
	}

	public ArmorWeightConfiguration getConfiguration() {
		return config;
	}

	public WeightManager getWeightManager() {
		return weightManager;
	}

	public ArmorWeightCommands getCommandExecutor() {
		return commands;
	}

	public ArmorWeightLanguage getLanguage() {
		return lang;
	}

	public void reload() {
		onDisable();
		onEnable();
	}

	public String getWebsite() {
		return getDescription().getWebsite();
	}

	public String getVersion() {
		return getDescription().getVersion();
	}
}
