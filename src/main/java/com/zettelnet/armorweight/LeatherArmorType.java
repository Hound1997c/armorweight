package com.zettelnet.armorweight;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class LeatherArmorType extends ArmorType {

	public LeatherArmorType(Material material) {
		super(material);
	}

	public LeatherArmorType(String name, Material... contentMaterials) {
		super(name, contentMaterials);
	}

	@Override
	public double getWeight(ItemStack item) {
		// TODO Make colored armor heavier
		return super.getWeight(item);
	}

}
