package com.zettelnet.armorweight;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.zettelnet.armorweight.event.PlayerWeightChangeEvent;

public class WeightTracker implements Listener {

	private final ArmorWeightPlugin plugin;
	private final WeightManager manager;
	private final ArmorWeightLanguage lang;
	private final ArmorWeightConfiguration config;

	private final Map<Player, Long> cooldownMap;

	private long cooldown;

	public WeightTracker(final ArmorWeightPlugin plugin, final WeightManager manager) {
		this.plugin = plugin;
		this.manager = manager;
		this.lang = plugin.getLanguage();
		this.config = plugin.getConfiguration();

		this.cooldownMap = new HashMap<>();
		this.cooldown = config.weightWarningCooldown();
	}

	public void register() {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit(PlayerQuitEvent event) {
		cooldownMap.remove(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerWeightChange(PlayerWeightChangeEvent event) {
		if (event.getNewWeight() <= event.getOldWeight()) {
			return;
		}
		if (!config.weightWarningEnabled()) {
			return;
		}
		
		Player player = event.getPlayer();
		long time = System.currentTimeMillis();
		boolean canSendMessage = cooldownMap.containsKey(player) ? time - cooldownMap.get(player) >= cooldown : true;

		if (canSendMessage) {
			lang.weightWarning.send(player, "weight", manager.formatWeight(event.getNewWeight()), "oldWeight", manager.formatWeight(event.getOldWeight()));
			cooldownMap.put(player, time);
		}
	}
}
